#!/usr/bin/env python

'''
Get notified when your friends go online in DDNet
'''

from argparse import ArgumentParser
from datetime import datetime
from pathlib import Path
import json
import logging
import os
import platform
import time

from notifypy import Notify
import requests
import psutil


def ps_running(name='DDNet'):
    '''
    check if process runs.
    return bool
    '''
    for proc in psutil.process_iter(['name']):
        if proc.info['name'] == name:
            logging.debug('%s is running', name)
            return True
    return False


def get_ddnet_dir():
    '''
    Get DDNet directory for different platforms
    '''
    # python 3.10+
    # match platform.system():
    #     case 'Windows':
    #         appdata = Path(os.getenv("APPDATA"))
    #         p1 = Path(appdata / 'DDNet')
    #         p2 = Path(appdata / 'Teeworlds')
    #     case 'Darwin':
    #         p1 = Path('~/Library/Application Support/DDNet').expanduser()
    #         p2 = Path('~/Library/Application Support/Teeworlds').expanduser()
    #     case 'Linux':
    #         p1 = Path('~/.local/share/ddnet').expanduser()
    #         p2 = Path('~/.teeworlds').expanduser()
    #     case _:
    #         logging.fatal('Unknown operating system')
    #         exit(1)

    p = platform.system()
    if p == 'Windows':
        appdata = Path(os.getenv("APPDATA"))
        p1 = Path(appdata / 'DDNet')
        p2 = Path(appdata / 'Teeworlds')
    elif p == 'Darwin':
        p1 = Path('~/Library/Application Support/DDNet').expanduser()
        p2 = Path('~/Library/Application Support/Teeworlds').expanduser()
    elif p == 'Linux':
        p1 = Path('~/.local/share/ddnet').expanduser()
        p2 = Path('~/.teeworlds').expanduser()
    else:
        logging.fatal('Unknown operating system')
        exit(1)

    if p1.is_dir():
        return p1
    if p2.is_dir():
        return p2


def get_friends(friends_file):
    '''
    return set with friends from friends_file (if set) or settings_ddnet.cfg
    '''
    ddnet_dir = get_ddnet_dir()
    if ddnet_dir is None:
        logging.error('DDNet directory not found')
        return []

    friends = set()

    if friends_file is not None:
        with open(Path(friends_file), encoding='utf-8') as f:
            lines = f.readlines()
        for line in lines:
            friends.add(line.rstrip('\n'))
    else:
        with open(ddnet_dir / 'settings_ddnet.cfg', encoding='utf-8') as f:
            lines = f.readlines()

        for line in lines:
            if line.startswith('add_friend '):
                friend = line.split(' ')[1].strip('"')
                if friend != '':
                    friends.add(friend)
    return friends


def get_online_friends(friends, url='https://master1.ddnet.org/ddnet/15/servers.json'):
    '''
    return set with online friends and dict with name and details about server and map
    '''
    r = requests.get(url, timeout=10)
    if r.status_code != 200:
        logging.error('url returned %s', r.status_code)

    j = r.json()
    online_friends_details = []
    online_friends = set()
    for server in j['servers']:
        # get server and map details
        servername = server['info']['name']
        mapname = server['info']['map']['name']
        clients = server['info']['clients']
        client_names = set()

        for client in clients:
            client_names.add(client['name'])

        online_friends_srv = set.intersection(client_names, friends)
        if len(online_friends_srv) > 0:
            online_friends.update(online_friends_srv)

            details = {
                'servername': servername,
                'mapname': mapname,
                'clients': len(client_names),
                'friends': online_friends_srv,
            }

            online_friends_details.append(details)

    for srv in online_friends_details:
        logging.debug(srv)
    logging.debug(online_friends)
    return online_friends, online_friends_details


def get_last_seen(file='last_seen.json', strptime=True):
    '''
    return obj with friends and last seen timestamp
    set strptime to False to use string instead of datetime obj
    '''
    try:
        with open(file, encoding='utf-8') as f:
            last_seen = json.load(f)
            if not strptime:
                return last_seen

            for friend in last_seen:
                dt = datetime.fromisoformat(last_seen[friend])
                last_seen[friend] = dt
            return last_seen

    except FileNotFoundError:
        return {}


def set_last_seen(online, file='last_seen.json'):
    '''
    save file with friends and last seen timestamp
    '''
    last_seen = get_last_seen(strptime=False)
    for friend in online:
        last_seen[friend] = datetime.now().isoformat(timespec='seconds')

    with open(file, 'w', encoding='utf-8') as f:
        json.dump(last_seen, f, indent=2)
    logging.debug('Saved %s', file)


def last_seen_diff(online):
    '''
    return obj with friends and seconds since last seen
    '''
    diff_all = {}
    last_seen = get_last_seen()
    for friend in online:
        if friend in last_seen:
            diff = datetime.now().replace(microsecond=0) - last_seen[friend]
            diff_all[friend] = diff
        else:
            diff_all[friend] = -1


def get_msgs(new_online, details):
    '''
    Create message strings from names and server details
    details = [{'servername': 'DDNet Argentina - Brutal', 'mapname': 'Nucl', 'clients': 2, 'friends': set()}, [...
    '''
    msgs = []
    for server in details:
        friends_new = set.intersection(new_online, server['friends'])
        if len(friends_new) == 0:
            continue

        # NAME is now online on GER3 Brutal MAP (3/12)
        # NAME and NAME2 are now online on GER3 Brutal MAP (3/12)
        # NAME, NAME2 and NAME3 are now online on GER3 Brutal MAP (3/12)

        # NAME part
        msg = ''
        l = len(friends_new)
        if l == 1:
            msg += f'{friends_new.pop()} is'
        elif l == 2:
            msg += f'{friends_new.pop()} and {friends_new.pop()} are'
        elif 2 < l <= 64:
            last = friends_new.pop()
            before_last = friends_new.pop()
            for friend in friends_new.pop():
                msg += f'{friend}, '
            msg += f'{before_last} and {last}'

        # are now online on GER3 Brutal MAP (3/12)
        msg += f" now online on {server['servername']}: {server['mapname']}"
        msg += f" ({len(server['friends'])}/{server['clients']})"
        msgs.append(msg)
    return msgs


def loop(friends, ddnet_needed, interval, notify, notify_always):
    '''
    Main loop with all args from main
    '''
    before_online = set()
    firstrun = True
    while True:
        ddnet_running = ps_running()
        if (ddnet_needed and ddnet_running) or not ddnet_needed:

            online, details = get_online_friends(friends)
            set_last_seen(online)

            new_online = set.difference(online, before_online)
            logging.debug('%s friends are online (%s new)', len(online), len(new_online))

            # new_offline = before_online - online
            before_online = online
            if not firstrun and len(new_online) > 0:
                msgs = get_msgs(new_online, details)
                for msg in msgs:
                    logging.info(msg)

                if ddnet_running:
                    with open(get_ddnet_dir() / 'fifo', 'w', encoding='utf-8') as f:
                        for msg in msgs:
                            # Everything after the number sign (#) get's filtered out by DDNet
                            f.write(f"echo {msg.replace('#', '＃')}\n")

                if notify_always or (ddnet_running and notify):
                    for msg in msgs:
                        notification = Notify()
                        notification.title = 'Online on DDNet'
                        notification.message = msg
                        notification.icon = ""
                        notification.send()

        firstrun = False
        logging.debug('sleeping %s', interval)
        time.sleep(interval)


def main():
    '''
    Parse args and call loop
    '''
    parser = ArgumentParser(description='Send notifications when your friends go online')
    parser.add_argument(
        '-v',
        '--verbosity',
        help='Set verbosity level (default: %(default)s)',
        choices=['warning', 'info', 'debug'],
        default='info',
    )
    parser.add_argument(
        '--file',
        help='Path to friends file with one name per line (if unset use friends from settings_ddnet.cfg)',
        metavar='PATH',
    )
    parser.add_argument('--ddnet', help='Only notify when DDNet is running', action='store_true')
    parser.add_argument(
        '--interval', help='Polling interval of servers.json  (default: %(default)s)', default=30, type=int
    )
    parser.add_argument('--notify', help='Send system notifications when DDNet is not running', action='store_true')
    parser.add_argument(
        '--notify-always', help='Send system notifications even when DDNet is running', action='store_true'
    )
    # parser.add_argument('--min-online', help='Only notify if more then X friends are online', default=0, type=int)
    # parser.add_argument(
    #     '--min-online-server', help='Only notify if more then X friends are online on one server', default=0, type=int
    # )

    args = parser.parse_args()
    logging.basicConfig(level=args.verbosity.upper(), format='%(asctime)s %(levelname)s %(message)s')
    logging.debug('Args: %s', vars(args))

    friends = get_friends(args.file)
    if len(friends) == 0:
        logging.error('Found 0 friends. :C')
        exit(1)

    logging.info('Found %s friends', len(friends))
    loop(friends, args.ddnet, args.interval, args.notify, args.notify_always)


if __name__ == '__main__':
    main()
