#!/usr/bin/env python

'''
Translate DDNet chat ingame by using log and fifo files
'''

from argparse import ArgumentParser
from datetime import datetime
from pathlib import Path

# import json
import logging
import os
import platform
import re
import time

# import requests
import psutil
from deepl import Translator, exceptions


def splitname(namemsg):
    'Split at ": " but only at first 17 chars (15 chars for name)'
    try:
        name, msg = namemsg[:17].split(': ', maxsplit=1)
        msg += namemsg[17:]
    except ValueError:
        name = ''
        msg = namemsg

    return name, msg


def ps_running(name='DDNet'):
    '''
    check if process runs.
    return bool
    '''
    for proc in psutil.process_iter(['name']):
        if proc.info['name'] == name:
            logging.debug('%s is running', name)
            return True
    return False


def get_ddnet_dir():
    '''
    Get DDNet directory for different platforms
    '''
    # python 3.10+
    # match platform.system():
    #     case 'Windows':
    #         appdata = Path(os.getenv("APPDATA"))
    #         p1 = Path(appdata / 'DDNet')
    #         p2 = Path(appdata / 'Teeworlds')
    #     case 'Darwin':
    #         p1 = Path('~/Library/Application Support/DDNet').expanduser()
    #         p2 = Path('~/Library/Application Support/Teeworlds').expanduser()
    #     case 'Linux':
    #         p1 = Path('~/.local/share/ddnet').expanduser()
    #         p2 = Path('~/.teeworlds').expanduser()
    #     case _:
    #         logging.fatal('Unknown operating system')
    #         exit(1)

    p = platform.system()
    if p == 'Windows':
        appdata = Path(os.getenv("APPDATA"))
        p1 = Path(appdata / 'DDNet')
        p2 = Path(appdata / 'Teeworlds')
    elif p == 'Darwin':
        p1 = Path('~/Library/Application Support/DDNet').expanduser()
        p2 = Path('~/Library/Application Support/Teeworlds').expanduser()
    elif p == 'Linux':
        p1 = Path('~/.local/share/ddnet').expanduser()
        p2 = Path('~/.teeworlds').expanduser()
    else:
        logging.fatal('Unknown operating system')
        exit(1)

    if p1.is_dir():
        return p1
    if p2.is_dir():
        return p2


def fifo(fifo_file, msg=None, cmd='echo'):
    if msg is None:
        logging.debug('FIFO cmd %s', cmd)
    else:
        logging.debug('FIFO %s %s', cmd, msg)
    with open(fifo_file, 'w', encoding='utf-8') as f:
        if msg:
            # Everything after the number sign (#) get's filtered out by DDNet
            f.write(f"{cmd} {msg.replace('#', '＃')}\n")
        else:
            f.write(f'{cmd}\n')


def translate(msgd, args, sending=False):
    dt = datetime.now() - msgd['date']
    msg = msgd['msg']
    if dt.seconds > args.past or not msg.startswith(args.prefix):
        return
    # logging.debug('msg %s', msg)
    name = msgd['name']
    target_lang = args.target_lang.upper()
    source_lang = None

    # send message
    if sending:
        # {optional prefix}{optional source lang->}{target lang} {message}
        # message CAN contain name: at beginning that should not be translated
        msg = msg[len(args.prefix) :]
        lang, msg = msg.split(' ', maxsplit=1)
        try:
            source_lang, target_lang = lang.split('->', maxsplit=1)
        except ValueError:
            target_lang = lang
        name, msg = splitname(msg)
        name = name.rstrip()
        msg = msg.lstrip()

    # received message
    # elif msgd['type'] == 'chat' or (msgd['type'] == 'whisper' and msgd['send_recv'] == 'recv'):

    logging.debug('translating %s: %s', name, msg)
    if args.translator == 'deepl':
        target_lang = 'EN-US' if target_lang == 'EN' else target_lang
        target_lang = 'PT-PT' if target_lang == 'PT' else target_lang
        try:
            t = Translator(args.apikey).translate_text(msg, target_lang=target_lang, source_lang=source_lang)
        except exceptions.DeepLException as e:
            logging.error(e)
            return
        result = f'{name}: {t.text}'
        info = f'[{t.detected_source_lang}]'
    elif args.translator == 'libretranslate':
        result = 'not implemented yet'
    if sending:
        fifo(args.fifo, result, cmd='chat all')
    elif t.detected_source_lang.upper() in args.exclude_lang:
        logging.info(f'SKIPPING {t.detected_source_lang} -- {args.exclude_lang}')
        return
    else:
        fifo(args.fifo, f'❤{info} {result}')


def watch(log, cmds):
    '''
    Watch and split up logfile
    '''
    with open(log, encoding='utf-8') as f:
        while True:
            # Once all lines are read this just returns ''
            # until the file changes and a new line appears
            line = f.readline()
            if line == '':
                yield {}
                if len(cmds.ready()) == 0:
                    time.sleep(0.5)
            else:
                m = re.match(r'([\d-]+ [\d:]+) I ([^:]+): (\*\*\* )?(.*)', line.rstrip('\n'))
                if m is None:
                    continue
                date, msg_type, srv_msg, msg = m.groups()

                date = datetime.strptime(date, '%Y-%m-%d %H:%M:%S')
                send_recv = None
                name = None
                if srv_msg is not None:
                    msg_type = 'server'
                elif msg_type == 'whisper':
                    m = re.match(r'(→|←) ([^:]+): (.*)', msg)
                    send_recv, name, msg = m.groups()
                    send_recv = 'send' if send_recv == '→' else 'recv'
                else:
                    name, msg = splitname(msg)

                yield {'date': date, 'type': msg_type, 'send_recv': send_recv, 'name': name, 'msg': msg}
                # print(msg_type, "srv:", srv_msg, "msg:", msg)


class Commands:
    '''
    cmd dict:
    cmd: cmd to execute in f1
    type: output to expect
        e.g. console for "2022-11-18 04:49:31 I console: Value: foo"
        None if output/result is not needed
    interval: run command every x seconds, None to run once
    last_run: datetime of last execution, None if never executed
    result: output string is saved here
    '''

    def __init__(self):
        c1 = {'cmd': 'player_name', 'type': 'console', 'interval': 60, 'last_run': None}
        c2 = {'cmd': 'dummy_name', 'type': 'console', 'interval': 60, 'last_run': None}
        self.cmds = [c1, c2]
        self.last_cmd = None

    def ready(self):
        rdy = []
        for cmd in self.cmds:
            if (
                cmd['last_run'] is None
                or cmd['interval'] is None
                or cmd['interval'] <= (datetime.now() - cmd['last_run']).total_seconds()
            ):
                if cmd['interval'] is None and cmd['last_run'] is not None:  # cmd was already run
                    continue
                rdy.append(cmd)
        return rdy

    def run(self):
        '''
        return a command that is ready and set last_run to now, else return None
        '''
        try:
            cmd = self.ready().pop()
        except IndexError:
            return
        self.last_cmd = cmd
        cmd['last_run'] = datetime.now()
        logging.debug('Running cmd: %s', cmd['cmd'])
        return cmd

    def add(self, cmd, intverval=60, last_run=None):
        _cmd = {'cmd': cmd, 'interval': intverval, 'last_run': last_run}
        self.cmds.append(_cmd)

    def last_cmd_result(self, result, date, accuracy=3):
        '''
        Match date with last_run and update result
        '''
        if self.last_cmd is None:
            logging.error('No last cmd found, make sure Command.run() was executed')
            return
        dt = abs((self.last_cmd['last_run'] - date).total_seconds())
        if dt > accuracy:
            logging.error('Command result does not seem to match to command. Time diff: %s', dt)
            return
        logging.debug('Result of cmd %s is %s', self.last_cmd['cmd'], result)
        self.last_cmd['result'] = result


def loop(args):
    '''
    Main loop with all args from main
    '''
    cmds = Commands()
    while True:
        if not ps_running():
            logging.debug("Waiting for DDNet to run")
            # logging.debug('sleeping %s', interval)
            time.sleep(10)
        else:
            prev_whisper = None
            # prev_datetime = None
            cmd_date = None
            for msg in watch(args.log, cmds):
                if msg == {}:
                    if cmd_date is not None:
                        continue
                    # run new command when no other messages arrive and result of last command is received
                    cmd = cmds.run()
                    if cmd is None:
                        continue
                    fifo(args.fifo, cmd=cmd['cmd'])
                    if cmd['type'] is not None:
                        cmd_date = datetime.now()
                    continue

                logging.debug(msg)
                # try:
                #     diff = (msg['date'] - prev_datetime).total_seconds()
                # except TypeError:
                #     diff = 0
                # logging.debug(f'last message before {diff}')
                # prev_datetime = msg.copy()['date']
                # check for whisper to self
                if cmd_date is not None:
                    logging.debug('cmd type: %s', cmd['type'])
                if cmd_date is not None and msg['type'] == cmd['type']:
                    cmds.last_cmd_result(msg['msg'], msg['date'])
                    cmd_date = None
                    continue
                # elif msg['type'] == 'console' and msg['name'] == 'Value' and cmd_date is not None:
                # result of many commands
                if msg['type'] == 'whisper':
                    if msg['send_recv'] == 'send':
                        prev_whisper = msg.copy()
                        # prev_whisper['send_recv'] = 'recv'
                    elif self_whisper(msg, prev_whisper):
                        translate(msg, args, sending=True)
                elif msg['type'] == 'chat' and not msg['msg'].startswith('— ❤'):
                    translate(msg, args)


def self_whisper(a, b):
    a = a.copy()
    b = b.copy()
    # {'date': date, 'type': msg_type, 'send_recv': send_recv, 'name': name, 'msg': msg}
    if 3 < abs((a['date'] - b['date']).total_seconds()):
        return False
    if a['send_recv'] == b['send_recv']:
        return False
    del a['date'], b['date']
    del a['send_recv'], b['send_recv']
    return a == b


def main():
    '''
    Parse args and call loop
    '''
    parser = ArgumentParser(description='Translate DDNet chat ingame')
    parser.add_argument(
        '-v',
        '--verbosity',
        help='Set verbosity level (default: %(default)s)',
        choices=['warning', 'info', 'debug'],
        default='debug',
    )
    parser.add_argument(
        '--fifo', help='Path to DDNet fifo file (default: "fifo" in ddnet directory)', metavar='PATH', type=Path
    )
    parser.add_argument(
        '--log', help='Path to DDNet log file (default: "ddnetlog" in ddnet directory)', metavar='PATH', type=Path
    )
    parser.add_argument(
        '--translator',
        help='Translator to use (default: %(default)s)',
        choices=['deepl', 'libretranslate'],
        default='deepl',
    )
    parser.add_argument('--apikey', help='API key (if needed) for translator')
    parser.add_argument(
        '--target-lang', help='Target language to translate incoming messages (default: %(default)s)', default='en'
    )
    parser.add_argument(
        '--prefix', help='Only send translation if message is prefixed with this (default: %(default)s)', default='""'
    )
    parser.add_argument(
        '--noconfirm', help='Send translated message directly instead of using `chat all`', action='store_true'
    )
    # parser.add_argument(
    #     '--exclude',
    #     help='Messages from these names are not translated (default: player and dummy name)',
    #     type=list,
    #     default=[],
    # )
    parser.add_argument(
        '--exclude-lang',
        help='Messages from these languages are not translated (comma seperated list) (default: %(default)s)',
        default='en',
    )
    parser.add_argument('--server', help='Libretranslate server')
    parser.add_argument(
        '--past',
        help='Translate messages of the last x seconds (default: %(default)s)',
        default=30,
        type=int,
        metavar='SECONDS',
    )

    args = parser.parse_args()
    args.exclude_lang = [l.upper() for l in args.exclude_lang.split(',')]
    args.fifo = get_ddnet_dir() / 'ddnet_client.fifo' if args.fifo is None else args.fifo
    args.log = get_ddnet_dir() / 'ddnet.log' if args.log is None else args.log
    # args.exclude =

    logging.basicConfig(level=args.verbosity.upper(), format='%(asctime)s %(levelname)s %(message)s')
    logging.debug('Args: %s', vars(args))

    loop(args)


if __name__ == '__main__':
    main()
